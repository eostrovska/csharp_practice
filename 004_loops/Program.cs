﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;


namespace _004_loops
{
    class Program
    {
        // task 4.1
        static void Main(string[] args)
        {
            Console.WriteLine("This loop shows how to display all numbers from 1 to 100");
            for (int i = 1; i < 101; i++)
            {
                Console.WriteLine(i);
            };
            Console.WriteLine(new string('-', 60));


            //Task 4.2
            List<int> add = new List<int>();
            for (int i = 1; i < 101; i++)
            {
                add.Add(i);
            }
            Console.WriteLine("NonGeneric Collections");
            ArrayList arrayList = new ArrayList();
            arrayList.AddRange(add);
            var v = arrayList[3];
        Start:
            Console.WriteLine(v);

            if (v != arrayList[3])
            {
                goto Start;
            }
            else
            {
                Console.WriteLine("Show numbers from ArrayList through loop 'for'.");
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine(arrayList[i]);
                }
            }
            Console.WriteLine("Show numbers from ArrayList through loop 'foreach'.");
            foreach (int f in arrayList)
            {
                try
                {
                    Console.WriteLine(arrayList[f]);
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Index was out of range. Must be non-negative and less than the size of the collection.");
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Argument out of range exeption.");
                }
            }
            Console.WriteLine("Show numbers from ArrayList backhanded");
            arrayList.Reverse();
            for (int i = 0; i < 100; i++)
            {
                Console.Write(arrayList[i] + ",");
            }
            Console.WriteLine("0");
            Console.WriteLine(new string('-', 60));
            var sortedList = new SortedList();
            for (int i = 0; i < 101; i++)
            {
                sortedList.Add(i, i);
            }
            Console.WriteLine("Show numbers from SortedList through loop 'foreach':");
            foreach (DictionaryEntry i in sortedList)
            {
                Console.WriteLine("Key = {0}, Value = {1}", i.Key, i.Value);
            }
            Console.WriteLine(new string('-', 60));
            Queue queue = new Queue();
            queue.Enqueue(add);
            Stack stack = new Stack();
            stack.Push(add);
            var hashTable = new Hashtable();
            for (int s = 0; s < 101; s++)
            {
                hashTable.Add(s, s);
            }
            Console.WriteLine("Show numbers from HashTable through loop 'foreach':");
            foreach (DictionaryEntry i in hashTable)
            {
                Console.WriteLine("Key={0}, Value={1}", i.Key, i.Value);
            }
            Console.WriteLine(new string('-', 60));
            int[] array = Enumerable.Range(1, 100).ToArray();
            Console.WriteLine("Show numbers from Array through loop 'for'.");
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(array[i]);
            }
            Console.WriteLine("Show numbers from Array through loop 'foreach'.");
            foreach (int g in array)
            {
                Console.WriteLine(g);
            }
            Console.WriteLine("Show numbers from Array backhanded");
            Array.Reverse(array);
            for (int i = 0; i < 100; i++)
            {
                Console.Write(array[i] + ",");
            }
            Console.WriteLine("0");
            Console.WriteLine(new string('-', 60));
            Console.WriteLine("Generic Collections");
            List<int> listOfT = new List<int>();
            listOfT.AddRange(add);
            Console.WriteLine("Show numbers from List<T> through loop 'for'.");
            for (int t = 0; t < 100; t++)
            {
                Console.WriteLine(listOfT[t]);
            }
            Console.WriteLine("Show numbers from List<T> through loop 'foreach'.");
            foreach (int y in listOfT)
            {
                try
                {
                    Console.WriteLine(listOfT[y]);
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Index was out of range. Must be non-negative and less than the size of the collection.");
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Argument out of range exeption.");
                }
            }
            Console.WriteLine("Show numbers from List<T> backhanded");
            listOfT.Reverse();
            for (int i = 0; i < 100; i++)
            {
                Console.Write(listOfT[i] + ",");
            }
            Console.WriteLine("0");
            Console.WriteLine(new string('-', 60));
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            for (int s = 1; s < 101; s++)
            {
                dictionary.Add(s, s);
            }
            Console.WriteLine("Show numbers from Dictionary<T,T> through loop 'foreach'.");
            foreach (KeyValuePair<int, int> i in dictionary)
            {
                Console.WriteLine("Key = {0}, Value = {1}", i.Key, i.Value);
            }
            Console.WriteLine("Show numbers from Dictionary<T,T> backhanded");
            Array dictionaryToArray = dictionary.ToArray();
            Array.Reverse(dictionaryToArray);
            foreach (KeyValuePair<int, int> i in dictionaryToArray)
            {
                Console.Write("Key = {0}, Value = {1}, ", i.Key, i.Value);
            }
            Console.WriteLine("Key = 0, Value = 0");
            Console.WriteLine(new string('-', 60));
            Console.WriteLine("This Collection consists of these collections:");
            System.Collections.ArrayList CollectiveColliction = new System.Collections.ArrayList();
            CollectiveColliction.Add(arrayList);
            CollectiveColliction.Add(listOfT);
            CollectiveColliction.Add(array);
            CollectiveColliction.Add(dictionary);
            CollectiveColliction.Add(queue);
            CollectiveColliction.Add(stack);
            CollectiveColliction.Add(hashTable);
            CollectiveColliction.Add(sortedList);
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine(CollectiveColliction[i]);
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("ArgumentOutOfRangeException");
            }


            // Task 4.7
            Console.WriteLine("This example shows how to create program which asks password until the correct password is entered");
            string password;
            do
            {
                Console.WriteLine("Enter correct password");
                password = Console.ReadLine();

                if (password != "pass")
                {
                    Console.WriteLine("You entered uncorrect password!");
                }
                else
                {
                    Console.WriteLine();
                }
            }
            while (password != "pass");
            Console.WriteLine("This password is correct!");
            Console.WriteLine(new string('-', 60));


            // Task 4.8
            int[] numbers1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            int[] numbers2 = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
            int[] sum;
            sum = new int[10];
            for (int element = 0; element < sum.Length; element++)
            {
                sum[element] = numbers1[element] + numbers2[element];
            }
            foreach (int i in sum)
            {
                Console.Write("{0} ", i);
            }







            //int[] numbers = { 4, 7, 13, 20, 33, 23, 54 };
            //int s = 0;

            //foreach (int el in numbers)
            //{
            //s += el;
            //}
            //Console.WriteLine(s);




            //Console.WriteLine("This loop shoes how to display all numbers wich are multiple of three. Loop covers a range of numbers from 1 to 58");
            //for (int i = 1; i <= 58; i += 3)
            //{
            //Console.WriteLine(i);
            //}
            //Console.WriteLine(new string('-', 60));





            //Console.WriteLine("This example shows how to display loop from 1 to 100 and how to display 'Fizz' instead numbers  wich are multiple of 3, ")
            //Console.Write("'Buzz' - inhead numbers wich are multiple of 5 and 'FizzBuzz' - inead numbers wich are multiple of 3 and 5");
            //for (int i = 1; i <= 100; i++)
            //{
            //if (i % 5 == 0 && i % 3 == 0)
            //{
            //Console.WriteLine("FizzBuzz");
            //}
            //else if (i % 3 == 0)
            //{
            //Console.WriteLine("Fizz");
            //}
            //else if (i % 5 == 0)
            //{
            //Console.WriteLine("Buzz");
            //}
            //else
            //{
            //Console.WriteLine(i);
            //}
            //}
            //Console.WriteLine(new string('-', 60));


            //Console.WriteLine("This loop shows nested loop");
            //for (int i = 0; i < 10; i++)
            //{
            //for (int j = 0; j < i; j++)
            //{
            //Console.Write("*");
            //}
            //Console.WriteLine();
            //}
            //Console.WriteLine(new string('-', 60));


            //Console.WriteLine("this example shows Dijkstra loop");
            //char ia = '\0';
            //for (;;)
            //{
            //ia = Console.ReadKey().KeyChar;

            //switch (ia)
            //{
            //case 'l':
            //{
            //Console.WriteLine("go");
            //continue;
            //}
            //case 'k':
            //{
            //Console.WriteLine("fly");
            //continue;
            //}
            //}
            //break;
            //}



            //int[] numbers = { 4, 7, 13, 20, 33, 23, 54, 46, 78 };
            //int s = 0;

            //foreach (int el in numbers)
            //{
            //if (el <= 50 && el >= 20)
            //Console.WriteLine(el);
            //}



            Console.ReadKey();
        }
    }
}
