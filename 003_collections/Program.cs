﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _003_collections
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> add = new List<int>();
            for (int i = 1; i < 101; i++)
            {
                add.Add(i);
            }
            Console.WriteLine("NonGeneric Collections");
            ArrayList arrayList = new ArrayList();
            arrayList.AddRange(add);
            //foreach (int number in arrayList)
            //{
            //Console.WriteLine(number);
            //}
            int[] array = Enumerable.Range(1, 100).ToArray();
            //foreach(int g in array)
            //{
            //Console.WriteLine(g);
            //}
            Queue queue = new Queue();
            queue.Enqueue(add);
            //Console.WriteLine("Count queue is {0}",queue.Count);
            Stack stack = new Stack();
            stack.Push(add);
            //Console.WriteLine("Count of stack is {0}", stack.Count);
            var hashTable = new Hashtable();
            for (int s = 1; s < 100; s++)
            {
                hashTable.Add(s, s);
            }
            //Console.WriteLine(hashTable[45]);
            var sortedList = new SortedList();
            for(int i = 0; i < 101; i++)
            {
                sortedList.Add(i,i);
            }
            foreach(DictionaryEntry i in sortedList)
            {
                Console.WriteLine("Key = {0}, Value = {1}", i.Key, i.Value);
            }
            Console.WriteLine("Generic Collections");
            List<int> list = new List<int>();
            list.AddRange(add);
            //foreach(int listNumber in list)
            //{
            //Console.WriteLine(listNumber);
            //}
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            for (int s = 1; s < 100; s++)
            {
                dictionary.Add(s, s);
            }
            //Console.WriteLine(dictionary[34]);











            //int[] array = new int[99];

            //foreach (int element in array)
            //{
            //Console.WriteLine(element);
            //}
            //Console.WriteLine();



            //int[] numbers = { 1, 10, 77, 33, 42, 75, 123, 23, 54 };
            //int sum = 0;
            //Console.WriteLine("The sum of all the numbers:");
            //foreach (int numeric in numbers)
            //{
            //sum = sum + numeric;
            //}
            //Console.WriteLine(sum);

            //Console.WriteLine("Length of the array:");
            //int size = numbers.Length;
            //Console.WriteLine(size);

            //int average = 0;
            //Console.WriteLine("Average of array:");
            //average = sum / size;
            //Console.WriteLine(average);

            //int sum1 = 0;
            //double average1 = 0;
            /* Console.WriteLine("Average of all numbers greater than 50  of array:");*/
            //var newNumbers = new List<int>();
            //foreach (int numeric1 in numbers)
            //{
            //if (numeric1 >= 50)
            //{
            //newNumbers.Add(numeric1);
            //sum1 = sum1 + numeric1;
            //}
            //}

            //average1 = (sum1 / newNumbers.Count);
            //Console.WriteLine(average1);





            //int[] numbers = { 1, 10, 77, 33, 42, 75, 123, 23, 54 };
            //int sum = 0;
            //Console.WriteLine("The sum of all the numbers:");
            //foreach (int numeric in numbers)
            //{
            //sum = sum + numeric;
            //}
            //Console.WriteLine(sum);

            //Console.WriteLine("Length of the array:");
            //int size = numbers.Length;
            //Console.WriteLine(size);

            //int average = 0;
            //Console.WriteLine("Average of array:");
            //average = sum / size;
            //Console.WriteLine(average);

            //int sum1 = 0;
            //int count = 0;
            //double average1 = 0;
            //Console.WriteLine("Average of all numbers greater than 50  of array:");
            //foreach (int numeric1 in numbers)
            //{
            //if (numeric1 >= 50)
            //{
            //sum1 = sum1 + numeric1;
            //count = count + 1;
            //}
            //}

            //average1 = (sum1 / count);
            //Console.WriteLine(average1);




            //int[] numbers = { 1, 10, 77, 33, 42, 75, 123, 23, 54 };
            //var threeMinNumbers = new List<int>();

            //var temp = numbers.Where(number => number > 70).OrderByDescending(number => number);

            //int min1 = numbers.Max();
            //int min2 = numbers.Max();
            //int min3 = numbers.Max();

            //foreach (int numeric in numbers)
            //{
            //if (numeric < min3)
            //{
            //min2 = min3;
            //min3 = numeric;
            //}
            //else if (numeric < min2)
            //{
            //min2 = numeric;
            //}
            //else if (numeric < min2)
            //{
            //min1 = min2;
            //min2 = numeric;
            //}
            //else if (numeric < min1)
            //{
            //min1 = numeric;
            //}
            //}

            //Console.WriteLine("Three smallest values in the numbers array from highest to lowest:");
            //Console.WriteLine(min1);
            //Console.WriteLine(min2);
            //Console.WriteLine(min3);

            //int indMin1 = Array.IndexOf(numbers, min1);
            //int indMin2 = Array.IndexOf(numbers, min2);
            //int indMin3 = Array.IndexOf(numbers, min3);

            //Console.WriteLine("Index  of the third lowest integer in numbers array is " + indMin1 + ";");
            //Console.WriteLine("Index of the second lowest integer in numbers array is " + indMin2 + ";");
            //Console.WriteLine("Index of the first lowest integer in numbers array is " + indMin3 + ";");

            //List<int> MinNumbers = new List<int>();
            //MinNumbers.Add(min3);
            //MinNumbers.Add(min2);
            //MinNumbers.Add(min1);

            //int size = MinNumbers.Count();
            //Console.WriteLine("length of the list is " + size + ";");

            //Console.WriteLine("MinNumbers array is sorted from highest to lowest:");
            //MinNumbers.Reverse();
            //foreach (int sor in MinNumbers)
            //{
            //Console.WriteLine(sor);
            //}





            //int[] myArray = new int[] { 0, 1, 2, 3, 13, 8, 5 };
            //int largest = int.MinValue;
            //int second = int.MinValue;
            //foreach (int i in myArray)
            //{
            //if (i > largest)
            //{
            //second = largest;
            //largest = i;
            //}
            //else if (i > second)
            //second = i;
            //}

            //Console.WriteLine(second);





            //int[,] array = new int[3, 3];
            //for (int i = 0; i < 3; i++)
            //{
            //for (int j = 0; j < 3; j++)
            //{
            //array[i, j] = i * j; 
            //}
            //}

            //for (int i = 0; i < 3; i++)
            //{
            //for (int j = 0; j < 3; j++)
            //{
            //Console.WriteLine("{0}", array[i, j]);
            //}
            //}



            //static int[] ModifyArray(int[] array, int modifier)
            //{ 
            //for(int i = 0; i < array.Length; i++)
            //{
            //array[i] = array[i] * modifier; 
            //}
            //return array;
            //}
            //static void Main()
            //{
            //int[] myArray = { 1, 2, 3, 4, 5, 6 };
            //myArray = ModifyArray(myArray, 5);
            //for(int i = 0; i < myArray.Length; i++)
            //{
            //Console.WriteLine("{0}", myArray[i]);
            //}



            Console.ReadLine();
        }
    }
}
