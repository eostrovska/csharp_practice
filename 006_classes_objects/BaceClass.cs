﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_classes_objects
{
    class BaceClass
    {
        public string publicField = "Public Fiel from Bace Class";
        private string privateField = "Private Fiel from Bace Class";
        protected string protectedField = "Protected Fiel from Bace Class";

        public BaceClass()
        {
            Console.WriteLine("Consructor from BaceClass");
        }

        public void Method()
        {
            Console.WriteLine("Bace");
        }
    }
}
