﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_classes_objects
{
    class DerivedClass : BaceClass
    {
        //Inheritance and key word bace:
        //public void DerivedMethod()
        //{
        //Console.WriteLine(publicField);// inheritance
        //Console.WriteLine(protectedField);// inheritance
        //}

        //public DerivedClass()
        //:base()// constructor was call from Bace Class
        //{
        //Console.WriteLine("Constructor frob Derived class");
        //}

        //public void DerivedMethod2()
        //{
        //Console.WriteLine("derived method 2");
        //}


        //polymorphism, upcast, downcast:
        public void Method()
        {
            Console.WriteLine("Derived");
        }




    }
}
