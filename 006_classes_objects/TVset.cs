﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_classes_objects
{
    class TVset
    {
        protected internal uint channel = 1;
        protected internal string producer;
        protected internal string mark;
        protected internal string screenType;
        protected internal float diagonal;
        protected internal string screenResolution;

        public void ShowTVSettings()
        {
            Console.WriteLine("Producing country is {0};", producer);
            Console.WriteLine("TV mark is {0};", mark);
            Console.WriteLine("Type of screen is {0};", screenType);
            Console.WriteLine("Diagonal is {0} inches;", diagonal);
            Console.WriteLine("Screen resolution is {0}", screenResolution);
        }
        public string SwitchingChannels()
        {
        Start:
            Console.WriteLine("Please choose how do you like to switch channels!");
            Console.WriteLine("If you want to switch channel via numbers - enter 1.");
            Console.WriteLine("If you want to switch channels via Next/Previous - enter 2.");
            var choiceOfSwiching = Console.ReadLine();
            int checkInt = 0;

            if (int.TryParse(choiceOfSwiching, out checkInt))
            {
                int choiceSwiching = Convert.ToInt32(choiceOfSwiching);

                switch (choiceSwiching)
                {
                    case 1:
                    StartByNumber:
                        Console.WriteLine("Enter number of channel, please or enter 'Stop', if you want to stop watching. You have 10 channels!");
                        var numberOfChannel = Console.ReadLine();

                        if (numberOfChannel.GetType() == typeof(string))
                        {
                            string channel = "";
                            switch (numberOfChannel)
                            {
                                case "1":
                                    channel = "BBC";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "2":
                                    channel = "Sport";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "3":
                                    channel = "ChildTime";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "4":
                                    channel = "Discowery";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "5":
                                    channel = "Film";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "6":
                                    channel = "Cook";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "7":
                                    channel = "Repairs";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "8":
                                    channel = "Fishing";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "9":
                                    channel = "News";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "10":
                                    channel = "Women's secrets";
                                    Console.WriteLine(channel);
                                    goto StartByNumber;
                                case "Stop":
                                    break;
                                default:
                                    Console.WriteLine("You entered incorrect number");
                                    goto StartByNumber;
                            }
                        }
                        else
                        {
                            Console.WriteLine("You entered incorrect number.");
                            goto StartByNumber;
                        }
                        return numberOfChannel;
                    case 2:
                        Console.WriteLine("Enter 'Next', if you want to watch next channel;");
                        Console.WriteLine("'Previous' - to watch previous;");
                    StartWithNextPre:
                        var typeOfChannel = Console.ReadLine();

                        if (typeOfChannel == "Next" && channel <= 10)
                        {
                            channel++;
                            if (channel == 11)
                            {
                                channel = 1;
                            }
                            else { };
                            Console.WriteLine("You are looking {0}th channel", channel);
                            goto StartWithNextPre;
                        }
                        else if (typeOfChannel == "Previous" && channel <= 10)
                        {
                            channel--;

                            if (channel == 0)
                            {
                                channel = 10;
                            }

                            Console.WriteLine("You are looking {0}th channel", channel);
                            goto StartWithNextPre;
                        }
                        else if (typeOfChannel == "Stop")
                        {
                            Console.WriteLine("Stop watching");
                        }
                        else
                        {
                            Console.WriteLine("You entered uncorrect value");
                            goto StartWithNextPre;
                        }
                        return Convert.ToString(channel);
                }
            }
            else
            {
                Console.WriteLine("You entered incorrect value.");
                goto Start;
            }
            return choiceOfSwiching;
        }
    }
}