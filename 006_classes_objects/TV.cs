﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_classes_objects
{
    class TV
    {
        protected string producer;
        protected string mark;
        protected string screenType;
        protected float diagonal;
        protected string screenResolution;

        public void ShowTVSettings()
        {
            Console.WriteLine("Producing country is {0};", producer);
            Console.WriteLine("TV mark is {0};", mark);
            Console.WriteLine("Type of screen is {0};", screenType);
            Console.WriteLine("Diagonal is {0} inches;", diagonal);
            Console.WriteLine("Screen resolution is {0}", screenResolution);
        }
        public string SwitchingChannels()
        {
            string channel = "";
            string choice = "";
            do
            {
                Start:
                    Console.WriteLine("Enter number of channel, please or enter 'Stop', if you want to stop watching");
                    Console.WriteLine("Enter 'Next', if you want to watch next channel;");
                    Console.WriteLine("'Previous' - to watch previous;");
                    int numberOfChannel = Console.Read();

                    if (numberOfChannel.GetType() != typeof(int))
                    {
                        Console.WriteLine("You entered wrong number of channel.");
                        goto Start;
                    }
                    else{ };

                Choice:
                    switch (numberOfChannel)
                    {
                        case 1:
                            channel = "BBC";
                            return channel;
                        case 2:
                            channel = "Sport";
                            return channel;
                        case 3:
                            channel = "ChildTime";
                            return channel;
                        case 4:
                            channel = "Discowery";
                            return channel;
                        case 5:
                            channel = "Film";
                            return channel;
                        case 6:
                            channel = "Cook";
                            return channel;
                        case 7:
                            channel = "Repairs";
                            return channel;
                        case 8:
                            channel = "Fishing";
                            return channel;
                        case 9:
                            channel = "News";
                            return channel;
                        case 10:
                            channel = "Women's secrets";
                            return channel;
                    }
                    switch (choice)
                    {
                        case "Next":
                            numberOfChannel++;
                            goto Choice;
                        case "Previous":
                            numberOfChannel--;
                            goto Choice;
                    }
                    return channel;
                }
            while (channel != "Stop");
        }
    }
}








                                                                                                       
