﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_interfaces
{
    class TransformerCar : ITransformerCar
    {
        public void Drive()
        {
            Console.WriteLine("Drive");
        }
        public void SafeTransportation()
        {
            Console.WriteLine("SafeTransportation");
        }
    }
}
