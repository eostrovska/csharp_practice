﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_interfaces
{
    class TransformerShipPlain : ITransformerPlane, ITransformerShip
    {
        public void Fly()
        {
            Console.WriteLine("Fly");
        }
        public void FastTransportation()
        {
            Console.WriteLine("FastTransportation");
        }
        public void Sail()
        {
            Console.WriteLine("Sail");
        }
        public void TransportationOfHugeLoad()
        {
            Console.WriteLine("TransportationOfHugeLoad");
        }
    }
}
