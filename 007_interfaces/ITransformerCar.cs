﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_interfaces
{
    interface ITransformerCar
    {
        void Drive();
        void SafeTransportation();
    }
}
