﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What does transformer - Car:");
            TransformerCar Car = new TransformerCar();
            Car.Drive();
            Car.SafeTransportation();
            Console.WriteLine("What does Transformer - Ship - Plane:");
            TransformerShipPlain ShipPlain = new TransformerShipPlain();
            ShipPlain.Sail();
            ShipPlain.TransportationOfHugeLoad();
            ShipPlain.Fly();
            ShipPlain.FastTransportation();
            Console.WriteLine("What does Transformer - Car - Ship - Plane:");
            TransformerCarShipPlain CarShipPlain = new TransformerCarShipPlain();
            CarShipPlain.Drive();
            CarShipPlain.SafeTransportation();
            CarShipPlain.Sail();
            CarShipPlain.TransportationOfHugeLoad();
            CarShipPlain.Fly();
            CarShipPlain.FastTransportation();
            Console.ReadKey();
        }
    }
}
