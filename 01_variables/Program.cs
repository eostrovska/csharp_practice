﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_variables
{
    class Program
    {
        static void Main(string[] args)
        {
            sbyte sbyteNumber1 = 46;
            sbyte sbyteNumber2 = 39;
            sbyte substraction = (sbyte)(sbyteNumber1 - sbyteNumber2);
            sbyte sum = (sbyte)(sbyteNumber1 + sbyteNumber2);
            Console.WriteLine("{1}-{2}={0}", substraction, sbyteNumber1, sbyteNumber2);
            Console.WriteLine("{1}+{2}={0}", sum, sbyteNumber1, sbyteNumber2);
            Console.WriteLine(new string('-', 50));

            bool isByteNumber2Bigger;
            bool isByteNumer3Bigger;
            byte byteNumber1 = 4;
            byte byteNumber2 = 6;
            byte byteNumber3 = 8;
            isByteNumber2Bigger = byteNumber1 < byteNumber2;
            isByteNumer3Bigger = (byteNumber1 + byteNumber2) < byteNumber3;
            byte division = (byte)(byteNumber3 / byteNumber1);
            Console.WriteLine("{1}<{2} is {0}", isByteNumber2Bigger, byteNumber1, byteNumber2);
            Console.WriteLine("({1}+{2})<{3} is {0}", isByteNumer3Bigger, byteNumber1, byteNumber2, byteNumber3);
            Console.WriteLine("{1}/{2}={0}", division, byteNumber3, byteNumber1);
            Console.WriteLine(new string('-', 50));
            
            short shortNumber1 = 996; 
            short shortNumber2 = 56;
            short shortNumber3 = -34;
            bool isShortNumber1EquallyShortNumber3;
            short sumShortNumber1ShortNumber2 = (short)(shortNumber1 + shortNumber2);
            short comparisonShortNumber1ShortNumber3 = (short)(shortNumber1 - shortNumber3);
            isShortNumber1EquallyShortNumber3 = shortNumber2 == shortNumber3;
            Console.WriteLine("{1}+{2}={0}", sumShortNumber1ShortNumber2, shortNumber1, shortNumber2);
            Console.WriteLine("{1}-{2}={0}", comparisonShortNumber1ShortNumber3, shortNumber1, shortNumber3);
            Console.WriteLine("{1}={2} is {0}", isShortNumber1EquallyShortNumber3, shortNumber2, shortNumber3);
            Console.WriteLine(new string('-', 50));

            ushort ushortNumber1 = 1089;
            ushort ushortNumber2 = 2367;
            ushort ushortNumber3 = (ushort)(++ushortNumber1);
            ushort ushortNumber4 = (ushort)(ushortNumber2--);
            Console.WriteLine("{1}++ ={0}", ushortNumber3, ushortNumber1);
            Console.WriteLine("{1}-- ={0}", ushortNumber4, ushortNumber2);
            Console.WriteLine(new string('-', 50));
            
            int intNumber1 = 91;
            int intNumber2 = 9;
            int intNumber3 = (int)Math.Pow(intNumber2, 2);
            int intNumber4 = (int)Math.Sqrt(intNumber1);
            Console.WriteLine("{1} squared is {0}", intNumber3, intNumber1);
            Console.WriteLine("{1} square is {0}", intNumber4, intNumber2);
            Console.WriteLine(new string('-', 50));
        
            uint uintNumber1 = 1000000;
            uint uintNumber2 = 2000000;
            uint uintNumber3 = uintNumber2 / uintNumber1;
            uint uintNumber4 = uintNumber2 % uintNumber1;
            Console.WriteLine("{1}/{2}={0}", uintNumber3, uintNumber2, uintNumber1);
            Console.WriteLine("{1}%{2}={0}", uintNumber4, uintNumber2, uintNumber1);
            Console.WriteLine(new string('-', 50));

            long longNumber1 = 46;
            long longNumber2 = 7890;
            long longNumber3 = -98765;
            long longNumber4 = longNumber2 * longNumber1;
            long longNumber5 = longNumber2 * longNumber3;
            bool isLongNumber3Bigger = longNumber3 > longNumber1;
            bool isSumOfLongNumber3LongNumber1Bigger = (longNumber3 + longNumber2) < (longNumber3 + longNumber1);
            Console.WriteLine("{1}*{2}={0}", longNumber4, longNumber2, longNumber1);
            Console.WriteLine("{1}*{2} is {0}", longNumber5, longNumber1, longNumber3);
            Console.WriteLine("{1}>{2} is {0}", isLongNumber3Bigger, longNumber3, longNumber1);
            Console.WriteLine("({1}+{2})<({1}+{3}) is {0}", isSumOfLongNumber3LongNumber1Bigger, longNumber3, longNumber2, longNumber1);
            Console.WriteLine(new string('-', 50));

            ulong ulongNumber1 = 1234567890;
            ulong ulongNumber2 = 987654321;
            ulong ulongNumber3 = (ulong)Math.Pow(ulongNumber1, 2);
            float floatUlongNumber4 = (float)Math.Sqrt(ulongNumber2);
            bool isUlongNumber1Bigger = ulongNumber1 >= ulongNumber2;
            Console.WriteLine("{1} squared is {0}", ulongNumber3, ulongNumber1);
            Console.WriteLine("{1} square is {0}", floatUlongNumber4, ulongNumber2);
            Console.WriteLine("{1}>={2} is {0}", isUlongNumber1Bigger, ulongNumber1, ulongNumber2);
            Console.WriteLine(new string('-', 50));

            float floatNumber1 = 6.34f;
            float floatNumber2 = 123456;
            float floatNumber3 = floatNumber2 / floatNumber1;
            float floatNumber4 = (float)Math.Pow(floatNumber1, 3);
            Console.WriteLine("{1}/{2}={0}", floatNumber3, floatNumber2, floatNumber1);
            Console.WriteLine("{1} squared is {0}", floatNumber4, floatNumber1);
            Console.WriteLine(new string('-', 50));

            double doubleNumber1 = 10D;
            double doubleNumber2 = 3D;
            double doubleNumber3 = (double)doubleNumber1 / doubleNumber2;
            double doubleNumber4 = (double)Math.Sqrt(doubleNumber1);
            Console.WriteLine("{1}/{2}={0}", doubleNumber3, doubleNumber1, doubleNumber2);
            Console.WriteLine("{1} squre is {0}", doubleNumber4, doubleNumber1);
            Console.WriteLine(new string('-', 50));

            double doubleNumber5 = 16456.5678D;
            double doubleNumber6 = 345.567;
            dynamic dynamicNumber1 = 456.567;
            dynamic dynamicNumber2 = doubleNumber5 % doubleNumber6;
            dynamic dynamicNumber3 = dynamicNumber1 / doubleNumber5;
            dynamic dynamicNumber4 = Math.Pow(doubleNumber6, 3);
            Console.WriteLine("{1}% {2} ={0}", dynamicNumber2, doubleNumber5, doubleNumber6);
            Console.WriteLine("{1}/{2} ={0}", dynamicNumber3, dynamicNumber1, doubleNumber5);
            Console.WriteLine("{1} squared is {0}", dynamicNumber4, doubleNumber6);
            Console.WriteLine(new string('-', 50));

            char charNumber1 = 'g';
            char charNumber2 = 'h';
            Console.WriteLine(charNumber1);
            Console.WriteLine(charNumber2);

            string stringValue1 = "do ";
            string stringValue2 = "4 ";
            string stringValue3 = "or ";
            string stringValue4 = "5 ";
            string stringValue5 = "tasks ";
            string stringValue6 = "today";
            string stringValue7 = stringValue1 + stringValue2 + stringValue3 + stringValue4 + stringValue5 + stringValue6;
            Console.WriteLine(stringValue7);


            /*
            string what = "What\tare\tyou\tdoing\t?";
            Console.WriteLine(what);
            Console.WriteLine(what.ToUpper());

            string why = "Why\nare\nyou\nhere\n?";
            Console.WriteLine(why);

            string empty = "";
            string withnull = null;
            string space = "\t";

            String.IsNullOrEmpty(empty);
            String.IsNullOrEmpty(withnull);

            String.IsNullOrWhiteSpace(empty);
            String.IsNullOrWhiteSpace(space);

            Console.WriteLine(String.IsNullOrEmpty(empty));
            Console.WriteLine(String.IsNullOrEmpty(withnull));
            Console.WriteLine(String.IsNullOrEmpty(why));

            Console.WriteLine(String.IsNullOrWhiteSpace(empty));
            Console.WriteLine(String.IsNullOrWhiteSpace(space));

            String.Compare(what, why);
            Console.WriteLine(String.Compare(what, why));

            if (why.Contains("Why"))
                Console.WriteLine("Yes");

            Console.WriteLine(why.IndexOf("are"));//4
            Console.WriteLine(why.IndexOf("here"));

            Console.WriteLine(why.StartsWith("Fly"));
            Console.WriteLine(why.StartsWith("Why"));
            Console.WriteLine(why.EndsWith("Fly"));
            Console.WriteLine(why.EndsWith("?"));

            string hy = "Hy, my !";
            Console.WriteLine(hy.Insert(7, "darling"));

            string aloha = "Aloha, my darling!";
            Console.WriteLine(aloha.Remove(5, 12));
            Console.WriteLine(aloha.Substring(7, 11));
            Console.WriteLine(aloha.Replace("darling", "beach"));

            string todoarray = "To do array";
            char[] array = todoarray.ToCharArray();
            Console.WriteLine(array);
            */
            Console.ReadLine();
        }
    }
}
