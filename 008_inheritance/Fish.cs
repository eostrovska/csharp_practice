﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance
{
    class Fish : BaseClass
    {
        protected internal string gills = "gills";
        public void ShowFishProperties()
        {
            covers = "scales";
            limbs = "flipper";
            Console.WriteLine("All fish have {0}, {1}, {2}.", covers, limbs, gills);
        }
        public void Swimming()
        {
            Console.WriteLine("All fish are able to swim");
        }
    }
}
