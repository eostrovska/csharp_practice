﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Fish FishClass = new Fish(); // base properties - 9, fish - 2
            Bird BirdClass = new Bird(); // base properties - 9, bird - 3
            Dog DogClass = new Dog(); // base properties - 9, dog - 3
            Human HumanClass = new Human(); // base - 9, human - 3
            Console.WriteLine("About Fish.");
            FishClass.DescriptionBaseProperties();
            FishClass.ShowFishProperties();
            FishClass.Breath();
            FishClass.Communication();
            FishClass.FeedOn();
            FishClass.Movement();
            FishClass.ProtectionTerritory();
            FishClass.Reproduction();
            FishClass.TakingCareOffspring();
            FishClass.Vision();
            FishClass.Swimming();
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("About birds.");
            BirdClass.DescriptionBaseProperties();
            BirdClass.ShowBirdProperties();
            BirdClass.Movement();
            BirdClass.ProtectionTerritory();
            BirdClass.Reproduction();
            BirdClass.TakingCareOffspring();
            BirdClass.Breath();
            BirdClass.Communication();
            BirdClass.FeedOn();
            BirdClass.Vision();
            BirdClass.LayEggs();
            BirdClass.Fly();
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("About dogs.");
            DogClass.DescriptionBaseProperties();
            DogClass.ShowDogProperties();
            DogClass.Vision();
            DogClass.TakingCareOffspring();
            DogClass.Reproduction();
            DogClass.ProtectionTerritory();
            DogClass.Movement();
            DogClass.FeedOn();
            DogClass.Communication();
            DogClass.Breath();
            DogClass.ToTrain();
            DogClass.ProtectionPeople();
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("About humans.");
            HumanClass.DescriptionBaseProperties();
            HumanClass.ShowHumanProperties();
            HumanClass.Breath();
            HumanClass.Communication();
            HumanClass.FeedOn();
            HumanClass.Movement();
            HumanClass.ProtectionTerritory();
            HumanClass.Reproduction();
            HumanClass.TakingCareOffspring();
            HumanClass.Vision();
            HumanClass.UprightWalking();
            HumanClass.Thinking();
            Console.ReadKey();
        }
    }
}
