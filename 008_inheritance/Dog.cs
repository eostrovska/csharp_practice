﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance
{
    class Dog : BaseClass
    {
        protected internal string mammals = "mammals";
        protected internal string quadrupeds = "quadrupeds";
        public void ShowDogProperties()
        {
            limbs = "dog feet";
            covers = "wool";
            Console.WriteLine("All dogs have {0}, {1}.", limbs, covers);
            Console.WriteLine("All dogs are {0} and names {1}.", mammals, quadrupeds);
        }
        public void ToTrain()
        {
            Console.WriteLine("All dogs to train.");
        }
        public void ProtectionPeople()
        {
            Console.WriteLine("All dogs can protect people.");
        }
    }
}
