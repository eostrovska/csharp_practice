﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance
{
    class BaseClass
    {
        protected internal string vertebrate = "vertebrate"; //позвоночные
        protected internal string limbs = "limbs"; //конечности
        protected internal string covers = "covers"; //покровы
        protected internal string head = "head";
        protected internal string muscles = "muscles";

        public void DescriptionBaseProperties()
        {
            Console.WriteLine("All animals are {0} and have {1}, {2}, {3}, {4}.", vertebrate, limbs, covers, head, muscles);
        }
        public void Breath() //дыхание
        {
            Console.WriteLine("All animals are abble to breathe.");
        }
        public void Reproduction()
        {
            Console.WriteLine("All animals are able to breed.");
        }
        public void Movement() // передвижение
        {
            Console.WriteLine("All animals are able to move around.");
        }
        public void FeedOn() //питаться
        {
            Console.WriteLine("All animals are able to eat.");
        }
        public void Communication()
        {
            Console.WriteLine("All animals are able to communicate.");
        }
        public void Vision() //зрение
        {
            Console.WriteLine("All animals are able to see.");
        }
        public void ProtectionTerritory()
        {
            Console.WriteLine("All animals defend their territory.");
        }
        public void TakingCareOffspring() //забота о потомстве
        {
            Console.WriteLine("All animals take care about their offspring.");
        }

    }
}
