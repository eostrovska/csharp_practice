﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance
{
    class Human : BaseClass
    {
        protected internal string mind = "mind";
        protected internal string intelligence = "intelligence";
        public void ShowHumanProperties()
        {
            covers = "hair";
            limbs = "arms and legs";
            Console.WriteLine("All people have {0}, {1}.", covers, limbs);
            Console.WriteLine("{0}, {1} differ humans from animals.", mind, intelligence);
        }
        public void Thinking()
        {
            Console.WriteLine("All people are able to think.");
        }
        public void UprightWalking() //вертикально ходить
        {
            Console.WriteLine("All people are able to walk upright.");
        }

    }
}
