﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance
{
    class Bird : BaseClass
    {
        protected internal string beak = "beak";
        public void ShowBirdProperties()
        {
            limbs = "wings";
            covers = "feathers";
            Console.WriteLine("All birds have {0}, {1}, {2}.", beak, limbs, covers);
        }
        public void Fly()
        {
            Console.WriteLine("All birds are able to fly");
        }
        public void LayEggs()
        {
            Console.WriteLine("All birds lay eggs");
        }
    }
}
