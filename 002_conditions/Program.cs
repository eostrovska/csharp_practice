﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("There are two numbers: 100 and 101. The largest number is");
            int numberOne = 100;
            int numberTwo = 101;

            if (numberOne > numberTwo)
            {
                Console.WriteLine(numberOne);
            }
            else if (numberTwo > numberOne)
            {
                Console.WriteLine(numberTwo);
            }

            /*
            var result = numberOne;

            if (numberOne < numberTwo)
            {
                result = numberTwo;
            }

            Console.WriteLine(result);
            */

            if (numberOne % 2 == 0 && numberTwo % 2 == 0)
            {
                Console.WriteLine("Hello World 2");
            }
            else if (numberOne % 50 == 0 || numberTwo % 50 == 0)
            {
                Console.WriteLine("Hello World 3");
            }

            if (numberOne % 200 != 0 || numberTwo % 200 != 0)
                Console.WriteLine("Hello World 4");

            string stringOne = "Hello";
            string stringTwo = "Hell";

            var result = stringOne.Length;
            if (stringOne.Length < stringTwo.Length)
            {
                Console.WriteLine("The longer word is {0} with {1} symbols.", stringTwo, stringTwo.Length);
            }
            else
            {
                Console.WriteLine("The longer word is {0} with {1} symbols.", stringOne, result);
            }

            /*
            int number;
            Console.WriteLine("Enter the number");
            number = Convert.ToInt32(Console.ReadLine());

            if (number % 7 == 0 && number % 3 == 0)
                Console.WriteLine("Number" + number + " - is multiple of 7 and of 3");
            else if (number % 3 == 0)
                Console.WriteLine("Number" + number + " - is multiple of 3");
            else if (number % 7 == 0)
                Console.WriteLine("Number" + number + " - is multiple of 7");
            else
                Console.WriteLine("Number" + number + " - is multiple no of 7 and no of 3");



            int number;
            Console.WriteLine("Enter the number");
            number = Convert.ToInt32(Console.ReadLine());

            if (number % 7 == 0 && number % 3 == 0)
                Console.WriteLine("Number" + number + " - is multiple of 7 and of 3");
            else if (number % 3 == 0)
                Console.WriteLine("Number" + number + " - is multiple of 3");
            else if (number % 7 == 0)
                Console.WriteLine("Number" + number + " - is multiple of 7");
            else
                Console.WriteLine("Number" + number + " - is multiple no of 7 and no of 3");


            int owner;
            Console.WriteLine("Goals of owner: ");
            owner = Convert.ToInt32(Console.ReadLine());
            int guests;
            Console.WriteLine("Goals of guests: ");
            guests = Convert.ToInt32(Console.ReadLine());

            if (owner > guests)
                Console.WriteLine("Owner is win");
            else if (owner < guests)
                Console.WriteLine("Guests is win");
            else
                Console.WriteLine("Draw");
                */

            Console.ReadKey();
        }
    }
}
