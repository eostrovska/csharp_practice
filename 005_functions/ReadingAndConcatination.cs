﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_functions
{
    class ReadingAndConcatination
    {
        public string first;
        public string second;
        public char separator;
        public Tuple<string, string, char> ReadingString()
        {
        Start:
            Console.WriteLine("Please, enter first string");
            var one = Console.ReadLine();
            Console.WriteLine("Please, enter second string");
            var two = Console.ReadLine();
            int test;
            char check;

        Start1:
            if (int.TryParse(one, out test) || int.TryParse(two, out test))
            {
                Console.WriteLine("You entered number.");
                goto Start;
            }

            if (char.TryParse(one, out check) || char.TryParse(two, out check))
            {
                Console.WriteLine("You entered symbol.");
                goto Start;
            }
            else
            {
                Console.WriteLine("Please, enter separartor, like ',' or '/' or space");
            }
            var tree = Console.ReadKey().KeyChar;
            Console.ReadKey();

            if (char.IsNumber(tree) || char.IsLetter(tree))
            {
                Console.WriteLine();
                Console.WriteLine("You entered uncorrect value.");
                goto Start1;
            }
            Console.WriteLine();
            var r = Tuple.Create<string, string, char>(one, two, tree);
            var first = r.Item1;
            var second = r.Item2;
            var separator = r.Item3;
            this.first = first;
            this.second = second;
            this.separator = separator;
            return r;
        }
        public string ConcatinationString(string first, string second, char separator)
        {
            var result = "";

            if (first.GetType() == typeof(string) && second.GetType() == typeof(string) && separator.GetType() == typeof(char))
            {
                result = (first + separator + second);
            }
            return result;
        }
    }
}

