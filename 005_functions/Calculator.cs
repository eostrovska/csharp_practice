﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_functions
{
    class Calculator
    {
        public double GetNumbersAndCount()
        {
            Start:
            Console.WriteLine("Enter, please, first number!");
            var first = Console.ReadLine();
            StartWithoutZero:
            Console.WriteLine("Enter, please, second number!");
            var second = Console.ReadLine();
            double one;
            double two;
            char control;

            Operations:
                if (double.TryParse(first, out one) && double.TryParse(second, out two))
                {
                    Console.WriteLine("Enter, please, SUM, if you want to summation"); 
                    Console.WriteLine("Enter, please, SUB, if you want to subtraction");
                    Console.WriteLine("Enter, please, DIV, if you want to division");
                    Console.WriteLine("Enter, please, MULT, if you want to multiplication");
                }
                else
                {
                    if (char.TryParse(first, out control) || char.TryParse(second, out control))
                    {
                        Console.WriteLine("You entered a symbol!");
                        goto Start;
                    }
                    Console.WriteLine("You entered uncorrect values!");
                    goto Start;
                }
            
            string operation = Console.ReadLine();
            double result;
                switch (operation)
                {
                    case "SUM":
                        result = one + two;
                        return result;
                    case "SUB":
                        result = one - two;
                        return result;
                    case "DIV":
                    if (two == 0)
                    {
                        Console.WriteLine("You can not divide by zero");
                        goto StartWithoutZero;
                    }
                        result = one / two;
                        return result;
                    case "MULT":
                        result = one * two;
                        return result;
                    default:
                        Console.WriteLine("You entered uncorrect operation!");
                        goto Operations;
                }
        }                                           
    }
}
