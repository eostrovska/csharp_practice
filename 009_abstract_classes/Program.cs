﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_abstract_classes
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleUser SimpleUser = new SimpleUser();
            Moderator Moderator = new Moderator();
            WarehouseManager WarehouseManager = new WarehouseManager();
            Administrator Administrator = new Administrator();
            Console.WriteLine("What can do simple user-customer:");
            SimpleUser.Entrance();
            SimpleUser.Search();
            SimpleUser.ViewThings();
            SimpleUser.Buy();
            SimpleUser.Exit();
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("What can do moderator:");
            Moderator.Entrance();
            Moderator.Search();
            Moderator.ViewThings();
            Moderator.Buy();
            Moderator.AddProducts();
            Moderator.ChangeProducts();
            Moderator.Exit();
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("What can do warehouse manager:");
            WarehouseManager.Entrance();
            WarehouseManager.Search();
            WarehouseManager.ViewThings();
            WarehouseManager.Buy();
            WarehouseManager.Exit();
            WarehouseManager.ControlNumbersAndProperty();
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("What can do administrator:");
            Administrator.Entrance();
            Administrator.Search();
            Administrator.ViewThings();
            Administrator.Buy();
            Administrator.Exit();
            Administrator.ModeratorFacilities();
            Administrator.WarehouseManagerFacilities();
            Administrator.Administration();
            Console.ReadKey();
        }
    }
}
