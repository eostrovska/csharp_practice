﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_abstract_classes
{
    class Administrator : UserBaseClass
    {
        public override void Entrance()
        {
            Console.WriteLine("Enter like administrator.");
        }
        public override void Search()
        {
            base.Search();
        }
        public override void ViewThings()
        {
            base.ViewThings();
        }
        public override void Buy()
        {
            Console.WriteLine("You can buy things at administrator discount");
        }
        public override void Exit()
        {
            base.Exit();
        }
        public void ModeratorFacilities()
        {
            Console.WriteLine("You can use all moderator's facilities.");
        }
        public void WarehouseManagerFacilities()
        {
            Console.WriteLine("You can use all warehouse manager's facilities.");
        }
        public void Administration()
        {
            Console.WriteLine("Full administration and remove site at all.");
        }
    }
}
