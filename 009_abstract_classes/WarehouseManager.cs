﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_abstract_classes
{
    class WarehouseManager : UserBaseClass
    {
        public override void Entrance()
        {
            Console.WriteLine("Enter like warehouse manager.");
        }
        public override void Search()
        {
            base.Search();
            Console.WriteLine("Also");
            Console.WriteLine("You can search sales statistics and number of products.");
        }
        public override void ViewThings()
        {
            base.ViewThings();
            Console.WriteLine("Also");
            Console.WriteLine("You can view sales statistics");
        }
        public override void Buy()
        {
            Console.WriteLine("You can buy things at warehouse manager's discount.");
        }
        public override void Exit()
        {
            base.Exit();
        }
        public void ControlNumbersAndProperty()
        {
            Console.WriteLine("Change number of products.");
            Console.WriteLine("Control conformity with property.");
        }
    }
}
