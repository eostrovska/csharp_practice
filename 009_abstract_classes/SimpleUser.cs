﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_abstract_classes
{
    class SimpleUser : UserBaseClass
    {
        public override void Entrance()
        {
            Console.WriteLine("Hello, dear customer. We welcome you!");
        }
        public override void Search()
        {
            base.Search();
        }
        public override void ViewThings()
        {
            base.ViewThings();
        }
        public override void Exit()
        {
            Console.WriteLine("Good buy, dear customer.");
        }
        public override void Buy()
        {
            base.Buy();
        }
    }
}
