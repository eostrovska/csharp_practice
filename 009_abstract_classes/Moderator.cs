﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_abstract_classes
{
    class Moderator : UserBaseClass
    {
        public override void Entrance()
        {
            Console.WriteLine("Enter like moderator.");
        }
        public override void Search()
        {
            base.Search();
            Console.WriteLine("Also");
            Console.WriteLine("You can search things that are not sold some period of time.");
        }
        public override void ViewThings()
        {
            base.ViewThings();
            Console.WriteLine("Also");
            Console.WriteLine("You can view things that are not visible some period of time.");
            Console.WriteLine("Or");
            Console.WriteLine("You can view things that are not sold for several years.");
        }
        public override void Buy()
        {
            Console.WriteLine("You can buy things at moderator discount.");
        }
        public override void Exit()
        {
            base.Exit();
        }
        public void AddProducts()
        {
            Console.WriteLine("You can add new things to the site for sale.");
        }
        public void ChangeProducts()
        {
            Console.WriteLine("You can change conditions of sale or product's properties.");
        }
    }
}
