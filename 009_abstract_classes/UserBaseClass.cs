﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_abstract_classes
{
    abstract class UserBaseClass
    {
        virtual public void Entrance()
        {
            Console.WriteLine("Enter like abstract user.");
        }
        virtual public void Exit()
        {
            Console.WriteLine("Exit like abstract user.");
        }
        virtual public void Search()
        {
            Console.WriteLine("Look for things like abstract user.");
        }
        virtual public void ViewThings()
        {
            Console.WriteLine("View site like abstract user.");
        }
        virtual public void Buy()
        {
            Console.WriteLine("Buy things like abstract user.");
        }

    }
}
