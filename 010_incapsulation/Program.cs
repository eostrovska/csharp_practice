﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _010_incapsulation
{
    enum EnumT
    {
        zero = 1,
        one = 4,
        two = 6,
        three = 45
    }
    class Program
    {
        static void Main(string[] args)
        {
            DateTime birthday = new DateTime(2017, 11, 25);
            DateTime now = DateTime.Now;
            TimeSpan left = birthday - now;
            Console.WriteLine(left.Days + "and " + left.TotalHours);

            Console.WriteLine("Enum");
            Console.WriteLine(EnumT.one);
            Console.WriteLine((byte)EnumT.one);
            bool resulte = Enum.IsDefined(typeof(EnumT), "zero");

            if (resulte == true)
            {
                Console.WriteLine(Enum.IsDefined(typeof(EnumT), "zero"));
            }
            byte a = 5, b = 8;
            Console.WriteLine(a & b);
            Console.WriteLine(a | b);
            Console.WriteLine(a ^ b);
            Console.ReadKey();
        }
    }
}
